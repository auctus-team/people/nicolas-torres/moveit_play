#!/bin/bash

set -xe

yapf3 -i *.py
roscore &
rosrun rviz rviz -d obstacle.rviz &
python -i obstacle_visualization.py
