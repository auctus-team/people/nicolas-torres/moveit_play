#!/bin/bash

set -xe

yapf3 -i *.py
roscore &
rosrun rviz rviz -d basic_controls.rviz &
python -i basic_controls.py
