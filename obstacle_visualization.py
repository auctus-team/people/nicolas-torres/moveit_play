#!/usr/bin/env python
"""
Copyright (c) 2011, Willow Garage, Inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Willow Garage, Inc. nor the names of its
      contributors may be used to endorse or promote products derived from
      this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR PROFITS OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
"""

import rospy
import copy

from interactive_markers.interactive_marker_server import *
from visualization_msgs.msg import *
import geometry_msgs.msg
from tf.broadcaster import TransformBroadcaster

from random import random
from math import sin
import pinocchio as pin
import numpy as np
import sys

server = None
br = None
counter = 0

POSE1_NAME = 'pose1'
POSE2_NAME = 'pose2'


def stringToMotion(mystr):
    return pin.Motion(np.array([float(x) for x in mystr.split()]))


def se3matrix(se3vec):
    return np.vstack([
        np.hstack([pin.skew(se3vec.angular), se3vec.linear[None].T]),
        np.zeros(4)
    ])


def MotionToTwistmsg(mot):
    msg = geometry_msgs.msg.Twist()
    msg.linear.x = mot.linear[0]
    msg.linear.y = mot.linear[1]
    msg.linear.z = mot.linear[2]

    msg.angular.x = mot.angular[0]
    msg.angular.y = mot.angular[1]
    msg.angular.z = mot.angular[2]
    return msg


def PointmsgToArray(position):
    return np.array([position.x, position.y, position.z])


def PosemsgToSE3(msg):
    curq = pin.Quaternion(
        np.array([
            msg.orientation.x, msg.orientation.y, msg.orientation.z,
            msg.orientation.w
        ]))
    pos = np.array([msg.position.x, msg.position.y, msg.position.z])
    return pin.SE3(curq.matrix(), pos)


def SE3ToPosemsg(SE3):
    msg = geometry_msgs.msg.Pose()
    p = SE3.translation
    msg.position.x, msg.position.y, msg.position.z = p[0], p[1], p[2]
    q = pin.Quaternion(SE3.rotation)
    msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w = q.x, q.y, q.z, q.w
    return msg


pose1, pose2 = None, None
pose1tf, pose2tf = None, None
pose1Publisher, pose2Publisher = None, None
logpose1Publisher, logpose2Publisher = None, None
deltaSubPublisher, deltaOriginPublisher = None, None
deltaSubJlogPublisher = None


def trajectoryCallback(msg):
    if pose1 is None or pose2 is None:
        return

    time = rospy.Time.now()
    br.sendTransform(pose1.translation, (0, 0, 0, 1.0), time, "pose1",
                     "base_link")
    br.sendTransform(pose2.translation, (0, 0, 0, 1.0), time, "pose2",
                     "base_link")

    def setHeaderFrameid(msg, frame_id='base_link'):
        msg.header.frame_id = frame_id
        return msg

    p1, p2 = setHeaderFrameid(
        geometry_msgs.msg.PoseStamped()), setHeaderFrameid(
            geometry_msgs.msg.PoseStamped())
    p1.pose, p2.pose = SE3ToPosemsg(pose1), SE3ToPosemsg(pose2)
    pose1Publisher.publish(p1)
    pose2Publisher.publish(p2)

    deltaOriginlog = pin.log(pose1.actInv(pose2))
    deltaOrigin = setHeaderFrameid(geometry_msgs.msg.TwistStamped(), 'pose1')
    deltaOrigin.twist = MotionToTwistmsg(deltaOriginlog)
    deltaOriginPublisher.publish(deltaOrigin)

    deltaSub = setHeaderFrameid(geometry_msgs.msg.TwistStamped(), 'pose2')
    deltaSub.twist = MotionToTwistmsg(pin.log(pose1) - pin.log(pose2))
    deltaSubPublisher.publish(deltaSub)

    # option 1
    pose2_with_pose1R = pin.SE3(pose1.rotation, pose2.translation)
    deltaSubJlogTwist = pin.Motion(
        -pin.Jexp6(pin.log(pose2_with_pose1R))
        @ (pin.log(pose2_with_pose1R) - pin.log(pose1)))

    # option 2
    # pose1log_zeroR = pin.log(pose1)
    # pose1log_zeroR.angular.fill(0)
    # pose2log_zeroR = pin.log(pose2)
    # pose2log_zeroR.angular.fill(0)
    # deltaSubJlogTwist = pin.Motion(
    #     pin.Jexp6(pose2log_zeroR) @ (pose2log_zeroR - pose1log_zeroR))

    deltaSubJlogTwistZ = pin.Motion.Zero()
    deltaSubJlogTwistZ.linear[2] = deltaSubJlogTwist.linear[2]
    deltaSubJlog = setHeaderFrameid(geometry_msgs.msg.TwistStamped(), 'pose2')
    deltaSubJlog.twist = MotionToTwistmsg(deltaSubJlogTwistZ)
    deltaSubJlog.header.frame_id = 'pose1'
    deltaSubJlogPublisher.publish(deltaSubJlog)

    t1, t2 = setHeaderFrameid(geometry_msgs.msg.TwistStamped(),
                              'pose1'), setHeaderFrameid(
                                  geometry_msgs.msg.TwistStamped(), 'pose2')
    t1.twist, t2.twist = MotionToTwistmsg(pin.log(pose1)), MotionToTwistmsg(
        pin.log(pose2))
    # print('pin.log(pose1)', pin.log(pose1))
    # print('t1.twist', t1.twist)
    logpose1Publisher.publish(t1)
    logpose2Publisher.publish(t2)


# def frameCallback(msg):
#     global counter, br
#     time = rospy.Time.now()
#     br.sendTransform((0, 0, sin(counter / 140.0) * 2.0), (0, 0, 0, 1.0), time,
#                      "base_link", "moving_frame")
#     counter += 1


def processFeedback(feedback):
    pos = PointmsgToArray(feedback.pose.position)
    orient = np.array([
        feedback.pose.orientation.x, feedback.pose.orientation.y,
        feedback.pose.orientation.z, feedback.pose.orientation.w
    ])
    if feedback.event_type == InteractiveMarkerFeedback.POSE_UPDATE:
        print('{control}@{name}: {position} | {orientation}'.format(
            control=feedback.control_name,
            name=feedback.marker_name,
            position=pos,
            orientation=orient,
        ))

        global pose1, pose2
        if POSE1_NAME in feedback.marker_name:
            pose1 = PosemsgToSE3(feedback.pose)
            print('updated {}'.format(POSE1_NAME))
        elif POSE2_NAME in feedback.marker_name:
            pose2 = PosemsgToSE3(feedback.pose)
            print('updated {}'.format(POSE2_NAME))
        else:
            print('unknown marker name:', feedback.marker_name)
    elif feedback.event_type == InteractiveMarkerFeedback.BUTTON_CLICK:
        print('{control}@{name}: BUTTON_CLICK}'.format(
            control=feedback.control_name, name=feedback.marker_name))
    elif feedback.event_type == InteractiveMarkerFeedback.MENU_SELECT:
        print('{control}@{name}: MENU_SELECT}'.format(
            control=feedback.control_name, name=feedback.marker_name))
    server.applyChanges()


def alignMarker(feedback):
    pose = feedback.pose

    pose.position.x = round(pose.position.x - 0.5) + 0.5
    pose.position.y = round(pose.position.y - 0.5) + 0.5

    rospy.loginfo(feedback.marker_name + ": aligning position = " +
                  str(feedback.pose.position.x) + "," +
                  str(feedback.pose.position.y) + "," +
                  str(feedback.pose.position.z) + " to " +
                  str(pose.position.x) + "," + str(pose.position.y) + "," +
                  str(pose.position.z))

    server.setPose(feedback.marker_name, pose)
    server.applyChanges()


def rand(min_, max_):
    return min_ + random() * (max_ - min_)


def makeMarker(marker_type=Marker.CUBE,
               xyz=[.15, .15, .15],
               colors=[0.5, 0.5, .5],
               alpha=1,
               pose=SE3ToPosemsg(pin.exp(stringToMotion('0 0 0 0 0 0')))):
    marker = Marker()
    marker.type = marker_type
    marker.pose = pose
    marker.scale.x = xyz[0]
    marker.scale.y = xyz[1]
    marker.scale.z = xyz[2]
    marker.color.r = colors[0]
    marker.color.g = colors[1]
    marker.color.b = colors[2]
    marker.color.a = alpha

    return marker


def saveMarker(int_marker):
    server.insert(int_marker, processFeedback)


#####################################################################
# Marker Creation
def interactiveMarkerControls(q,
                              im_name,
                              interaction_modes=[
                                  InteractiveMarkerControl.ROTATE_AXIS,
                                  InteractiveMarkerControl.MOVE_AXIS
                              ],
                              fixed=False):
    q.normalize()
    controls = []
    for im in interaction_modes:
        control = InteractiveMarkerControl()
        control.orientation.w = q.w
        control.orientation.x = q.x
        control.orientation.y = q.y
        control.orientation.z = q.z

        im_str = None
        if im == InteractiveMarkerControl.ROTATE_AXIS:
            im_str = 'rotate'
        elif im == InteractiveMarkerControl.MOVE_AXIS:
            im_str = 'move'
        if im_str is None:
            print('Error: unknown interaction mode:', im)
            sys.exit(1)
        control.name = '{}_{}'.format(im_str, im_name)
        control.interaction_mode = im
        if fixed:
            control.orientation_mode = InteractiveMarkerControl.FIXED
        controls.append(control)
    return controls


def normalizeQuaternion(quaternion_msg):
    norm = quaternion_msg.x**2 + quaternion_msg.y**2 + quaternion_msg.z**2 + quaternion_msg.w**2
    s = norm**(-0.5)
    quaternion_msg.x *= s
    quaternion_msg.y *= s
    quaternion_msg.z *= s
    quaternion_msg.w *= s


def make6DofMarker(interaction_mode, pose, name, marker, fixed=False):
    int_marker = InteractiveMarker()
    int_marker.header.frame_id = "base_link"
    int_marker.pose = pose
    int_marker.scale = 1

    int_marker.name = name
    # int_marker.description = "Simple 6-DOF Control"
    int_marker.description = name

    # insert a box

    control = InteractiveMarkerControl()
    # control.always_visible = True
    control.markers.append(marker)
    control.interaction_mode = interaction_mode
    int_marker.controls.append(control)

    if interaction_mode == InteractiveMarkerControl.MOVE_ROTATE_3D:
        int_marker.controls += interactiveMarkerControls(pin.Quaternion(
            1, 0, 0, 1),
                                                         im_name='x',
                                                         fixed=fixed)
        int_marker.controls += interactiveMarkerControls(pin.Quaternion(
            0, 1, 0, 1),
                                                         im_name='y',
                                                         fixed=fixed)
        int_marker.controls += interactiveMarkerControls(pin.Quaternion(
            0, 0, 1, 1),
                                                         im_name='z',
                                                         fixed=fixed)
    server.insert(int_marker, processFeedback)


if __name__ == "__main__":
    rospy.init_node("basic_controls")

    br = TransformBroadcaster()
    # rospy.Timer(rospy.Duration(0.01), frameCallback)

    server = InteractiveMarkerServer("basic_controls")

    # pose0 = SE3ToPosemsg(pin.exp(stringToMotion('1 2 2 0 0 0')))
    # make6DofMarker(interaction_mode=InteractiveMarkerControl.NONE,
    #                pose=pose0,
    #                name='none',
    #                marker=makeMarker(
    #                    marker_type=Marker.ARROW,
    #                    xyz=[1, .05, .05],
    #                    colors=[1, 0, 0],
    #                    pose=SE3ToPosemsg(
    #                        pin.exp(stringToMotion('0 0 0 1 1.5 0')))))

    # poser = stringToMotion(
    #     '0.8173444   0.60327331 -0.11307027  2.51256546 -1.07595846  0.05600359'
    # )
    # posel = stringToMotion(
    #     '0.10615261 -1.00983103  0.37681411 -2.6008344   1.04078695  0.36898001'
    # )

    # pose1 = pin.SE3(np.eye(3), np.array([0,0,0]))
    pose1 = pin.exp(stringToMotion('0 0 2 0 0 0'))
    pose = SE3ToPosemsg(pose1)
    make6DofMarker(interaction_mode=InteractiveMarkerControl.MOVE_ROTATE_3D,
                   pose=pose,
                   name=POSE1_NAME,
                   marker=makeMarker(),
                   fixed=True)

    # pose2 = pin.SE3(np.eye(3), np.array([0,1,1]))
    pose2 = pin.exp(stringToMotion('0 0 0 0 0 0'))
    pose = SE3ToPosemsg(pose2)
    make6DofMarker(interaction_mode=InteractiveMarkerControl.MOVE_ROTATE_3D,
                   pose=pose,
                   name=POSE2_NAME,
                   marker=makeMarker(),
                   fixed=True)

    deltaOriginPublisher = rospy.Publisher('deltaOrigin',
                                           geometry_msgs.msg.TwistStamped,
                                           queue_size=1)

    deltaSubPublisher = rospy.Publisher('deltaSub',
                                        geometry_msgs.msg.TwistStamped,
                                        queue_size=1)

    deltaSubJlogPublisher = rospy.Publisher('deltaSubJlog',
                                            geometry_msgs.msg.TwistStamped,
                                            queue_size=1)

    pose1Publisher = rospy.Publisher('pose1',
                                     geometry_msgs.msg.PoseStamped,
                                     queue_size=1)
    pose2Publisher = rospy.Publisher('pose2',
                                     geometry_msgs.msg.PoseStamped,
                                     queue_size=1)
    logpose1Publisher = rospy.Publisher('logpose1',
                                        geometry_msgs.msg.TwistStamped,
                                        queue_size=1)
    logpose2Publisher = rospy.Publisher('logpose2',
                                        geometry_msgs.msg.TwistStamped,
                                        queue_size=1)

    pose1tf = TransformBroadcaster(),
    pose2tf = TransformBroadcaster(),
    rospy.Timer(rospy.Duration(0.1), trajectoryCallback)

    # position = geometry_msgs.msg.Point(-3, -6, 0)
    # makePanTiltMarker( position )
    # position = geometry_msgs.msg.Point( 0, -6, 0)
    # makeMovingMarker( position )

    server.applyChanges()

    rospy.spin()
