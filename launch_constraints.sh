#!/bin/bash

set -xe

yapf -i *.py
roscore &
rosrun rviz rviz -d constraints.rviz &
python -i constraints_visualization.py
