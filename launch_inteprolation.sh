#!/bin/bash

set -xe

yapf3 -i *.py
roscore &
rosrun rviz rviz -d interpolation.rviz &
python -i interpolation_visualization.py
